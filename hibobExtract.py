#!/bin/python

import json
import requests
import re

# Prompt the user for an API token
apiToken = input('Please enter your hibob API token: ')

# Define the base URL for the hibob API
baseUrl = 'https://api.hibob.com/v1'

# make an http call to the hibob API at the `/people' endpoint including the ?includeHumanReadbale=true parameter
# pass the api token in the Authorization header and accept application/json MIME type
result = requests.get(
  f'{baseUrl}/people?includeHumanReadable=true',
  headers={
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': apiToken
  }
)

# parse the result as JSON
people = result.json()

# create empty arrays for the Tribes, Services and Squads
tribes = []
services = []
squads = []

# loop through the people and extract the Tribe, Service and Squad from the humanReadable object, and store in lower case
for person in people['employees']:
  columns = person['humanReadable']['work']['customColumns']

  Tribe = re.sub(r'[,&]', '', columns['column_1625578971379']) if 'column_1625578971379' in columns else 'NOT DEFINED'
  Service = re.sub(r'[,&]', '', columns['column_1625579107971']) if 'column_1625579107971' in columns else 'NOT DEFINED'
  Squad = re.sub(r'[,&]', '', columns['column_1625579249980']) if 'column_1625579249980' in columns else 'NOT DEFINED'

  # append to the arrays if they're not already present
  if Tribe.lower() not in tribes:
    if Tribe.lower() != 'not defined':
      tribes.append(Tribe.lower())

  if Service.lower() not in services:
    if Service.lower() != 'not defined':
      services.append(Service.lower())

  if Squad.lower() not in squads:
    if Squad.lower() != 'not defined':
      squads.append(Squad.lower())

# sort the arrays
tribes.sort()
services.sort()
squads.sort()

# write the arrays to a json file
with open('tribes.json', 'w') as f:
  json.dump(tribes, f)

with open('services.json', 'w') as f:
  json.dump(services, f)

with open('squads.json', 'w') as f:
  json.dump(squads, f)
