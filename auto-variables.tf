# AUTO-GENERATED FILE - DO NOT EDIT
# This file is generated using the "sync-tags.py" script

variable "organization" {
  type        = string
  description = "The organization name"
  default     = "Genomics England Ltd"
  validation {
    condition = contains([
      "Genomics England Ltd",
    ], var.organization)
    error_message = "The organization value must be one of Genomics England Ltd."
  }
}

variable "tribe" {
  type        = string
  description = "The tribe name"
  validation {
    condition = contains([
      "cross tribe",
      "empowering our people",
      "engaging to build trust",
      "evolving genomic healthcare",
      "research ecosystem",
      "scalable tech",
      "the gel way",
    ], lower(var.tribe))
    error_message = "The tribe value must be one of cross tribe, empowering our people, engaging to build trust, evolving genomic healthcare, research ecosystem, scalable tech, the gel way."
  }
}

variable "gel_service" {
  type        = string
  description = "The gel_service name"
  validation {
    condition = contains([
      "best practise",
      "bioinformatics pipelines",
      "build research tools",
      "cross tribe",
      "define and deliver scientific research",
      "delivering scalable tech",
      "engage research scientists",
      "help clinicians order genomic tests",
      "help colleagues perform",
      "newborns",
      "trustworthy stewardship of genomic data",
    ], lower(var.gel_service))
    error_message = "The gel_service value must be one of best practise, bioinformatics pipelines, build research tools, cross tribe, define and deliver scientific research, delivering scalable tech, engage research scientists, help clinicians order genomic tests, help colleagues perform, newborns, trustworthy stewardship of genomic data."
  }
}

variable "squad" {
  type        = string
  description = "The squad name"
  validation {
    condition = contains([
      "accelerating data exploration",
      "bioinformatics consulting team",
      "business systems team",
      "cancer dss",
      "cancer long reads squad",
      "cancer short reads squad",
      "clinical variant ark squad",
      "cohort management team",
      "commercial procurement team",
      "communications team",
      "core healthcare team",
      "core multi-modal team",
      "core newborns team",
      "core research tools team",
      "core tech team",
      "cri and airlock team",
      "cross tribe",
      "cyber security squad",
      "diverse data",
      "dss platform",
      "engage scientists core team",
      "engagement team",
      "enterprise data squad",
      "estates team",
      "ethics legal and ig team",
      "executive support team",
      "finance team",
      "healthcare test ordering squad",
      "imaging data products squad",
      "infrastructure services squad",
      "internal service management team",
      "knowledge management squad",
      "logistics team",
      "newborns intake squad",
      "partnership development team",
      "people team",
      "performance team",
      "pipeline core dataflows squad",
      "pipeline orchestration squad",
      "pipelines cross-squad",
      "quality team",
      "rare disease dss",
      "rare disease interpretation portal squad",
      "rare disease pipeline squad",
      "re platform squad",
      "research acquisition and processing squad",
      "research data layer squad",
      "research data products",
      "research insight and facilitation",
      "research management team",
      "sample and data management squad",
      "scientific rd team",
      "scientific research core team",
      "the gel way squad",
    ], lower(var.squad))
    error_message = "The squad value must be one of accelerating data exploration, bioinformatics consulting team, business systems team, cancer dss, cancer long reads squad, cancer short reads squad, clinical variant ark squad, cohort management team, commercial procurement team, communications team, core healthcare team, core multi-modal team, core newborns team, core research tools team, core tech team, cri and airlock team, cross tribe, cyber security squad, diverse data, dss platform, engage scientists core team, engagement team, enterprise data squad, estates team, ethics legal and ig team, executive support team, finance team, healthcare test ordering squad, imaging data products squad, infrastructure services squad, internal service management team, knowledge management squad, logistics team, newborns intake squad, partnership development team, people team, performance team, pipeline core dataflows squad, pipeline orchestration squad, pipelines cross-squad, quality team, rare disease dss, rare disease interpretation portal squad, rare disease pipeline squad, re platform squad, research acquisition and processing squad, research data layer squad, research data products, research insight and facilitation, research management team, sample and data management squad, scientific rd team, scientific research core team, the gel way squad."
  }
}

variable "service" {
  type        = string
  description = "The service name"
}

variable "application" {
  type        = string
  description = "The application name"
  default     = ""
}

variable "environment" {
  type        = string
  description = "The environment name"
  validation {
    condition = contains([
      "core",
      "prod",
      "preprod",
      "uat",
      "e2e",
      "test",
      "dev",
      "sandbox",
    ], lower(var.environment))
    error_message = "The environment value must be one of core, prod, preprod, uat, e2e, test, dev, sandbox."
  }
}

variable "additional_tags" {
  type        = map(string)
  description = "Additional tags to apply to the resources"
  default     = {}
}
