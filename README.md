# Genomics England Standard Taging Module

This module can be used to ensure consistent and correct tagging of AWS resources, and the creation of tagging SCPs in the root of the Organization

> **IMPORTANT:** This module should *always* be used with a version of `latest`.
> The `latest` tag will target the latest tagging standards.
> Tags other than `latest` may be removed at any time.

## Note on Tag Hierarchy

Tags naturally form a hierarchy.  It is important to follow this convention to ensure that the tags are useful and consistent across the whole Genomics England AWS estate:

| Tag | Description | Example |
|-----|-------------|---------|
| **Organization** | The name of the Organization, | "Genomics England Ltd" |
| **GEL Service** | The name of the GEL Service, | "help clinicians order genomic tests" |
| **Tribe** | The name of the Tribe, | "Evolving Genomic Healthcare" |
| **Squad** | The name of the Squad, | "Core Tech" |
| **Service** | The name of the Service, | "Example Service" |
| **Application** | The name of the Application, | "Example Application" |
| **Environment** | The name of the Environment, | "Dev" |

## Usage

### Define the tags for this project

```hcl
module "tags" {
  source = "../"

  organization = "Genomics England Ltd"
  tribe        = "Evolving Genomic Healthcare"
  gel_service  = "help clinicians order genomic tests"
  squad        = "Core Tech"
  service      = "Example Service"
  application  = "Example Application"
  environment  = "Dev"
  additional_tags = {
    "MyTag" = "MyValue"
  }
}
```

### Automatically assign tags to all resources created using this provider

```hcl
provider "aws" {
  region = "eu-west-2"

  default_tags {
    tags = module.tags.tags
  }
}
```

### Add propagated tags to EC2 autoscaled resources

```hcl
resource "aws_autoscaling_group" "example" {

  ...

  # Provide propagated tags as a list rather than a set of attribute blocks
  tag = module.tags.propagated_tags
}
```

## IMPORTANT: S3 Bucket Objects

There is an issue with tagging S3 objects because AWS limits Objects to just 10 tags (the usual limit is 50).

We do not enforce tagging of objects, however `default_tags` on the AWS provider will automatically cause objects created with Terraform to be tagged.

Version 5.24.0 of the Terraform AWS provider adds a new attribute to the `aws_bucket_object` resource called `override_provider` which allows you to override the default tags.

```hcl
resource "aws_s3_object" "example" {
  bucket = aws_s3_bucket.example.id
  key    = "example"
  source = "example"

  override_provider {
    default_tags {
      tags = {}
    }
  }
}
```

This will override default tags and add no tags to the object.

> NOTE: this does not work with the `aws_s3_bucket_object` resource, only the newer `aws_s3_object` resource.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| tribe | The tribe name | `string` | n/a | yes |
| squad | The squad name | `string` | n/a | yes |
| service | The service name | `string` | n/a | yes |
| application | The application name | `string` | n/a | yes |
| environment | The environment name | `string` | n/a | yes |
| additional\_tags | A map of additional tags to add to the resource | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| tags | A map of tags |
| propagated\_tags | A list of tags to propagate to autoscaled resources |
| tag\_values\_policy | The tag values SCP policy document in json |
| organizations | The list of allowed Organizations |
| organization | The value of the organization in this tag set |
| tribes | List of allowed Tribes |
| tribe | The selected Tribe |
| gel\_services | List of allowed GEL Services |
| gel\_service | The selected GEL Service |
| services | The selected Service |
| squads | List of allowed Squads |
| squad | The selected Squad |
| application | The selected Application |
| environments | List of allowed Environments |
| environment | The selected Environment |
| additional\_tags | The supplied additional tags |
| datadog\_env\_vars | A map of environment variables for the Datadog agent |
