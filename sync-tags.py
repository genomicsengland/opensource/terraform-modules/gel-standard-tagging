#!/bin/python
import json

# define the tags values here as we cannot currently read from Semarchy

organizations = ["Genomics England Ltd"]

# read in tribes.json
with open('tribes.json', 'r') as f:
  tribes = json.load(f)

# read in services.json
with open('services.json', 'r') as f:
  gel_services = json.load(f)

# read in squads.json
with open('squads.json', 'r') as f:
  squads = json.load(f)

# Predefined Environments
environments = [
  "core",
  "prod",
  "preprod",
  "uat",
  "e2e",
  "test",
  "dev",
  "sandbox",
]

# consistently write a list in source.tf
def write_local(f, name, list):
  f.write(f'''    {name} = [
{"\n".join([f'      "{item}",' for item in list])}
    ]

''')

# consistently write a variable with validation
def write_validated_variable(f, name, list, lower = False, default = ""):
  f.write(f'''variable "{name}" {{
  type        = string
  description = "The {name} name"\n''')

  if default != "":
    f.write(f'  default     = "{default}"\n')

  f.write(f'''  validation {{
    condition = contains([
{"\n".join(f'      "{item}",' for item in list)}
    ], {"lower(var." + name + ")" if lower else "var." + name})
    error_message = "The {name} value must be one of {", ".join(list)}."
  }}
}}
\n''')

# consistently write a variable without validation
def write_variable(f, name, optional = False):
  f.write(f'''variable "{name}" {{
  type        = string
  description = "The {name} name"
{"  default     = \"\"\n}" if optional else "}"}

''')

def write_header(f):
  f.write('# AUTO-GENERATED FILE - DO NOT EDIT\n')
  f.write('# This file is generated using the "sync-tags.py" script\n\n')

# write the "source.tf" file
with open('auto-source.tf', 'w') as f:
  write_header(f)
  f.write('locals {\n')
  f.write('  source = {\n')

  write_local(f, 'Organization', organizations)
  write_local(f, 'Tribe', tribes)
  write_local(f, 'GEL_Service', gel_services)
  write_local(f, 'Squad', squads)
  write_local(f, 'Environment', environments)

  f.write('  }\n')
  f.write('}\n')

# write the "variables.tf" file
with open('auto-variables.tf', 'w') as f:
  write_header(f)
  write_validated_variable(f, 'organization', organizations, False, "Genomics England Ltd")
  write_validated_variable(f, 'tribe', tribes, True)
  write_validated_variable(f, 'gel_service', gel_services, True)
  write_validated_variable(f, 'squad', squads, True)
  write_variable(f, 'service')
  write_variable(f, 'application', True)
  write_validated_variable(f, 'environment', environments, True)

  # write the `additional_tags` variable
  f.write('variable "additional_tags" {\n')
  f.write('  type        = map(string)\n')
  f.write('  description = "Additional tags to apply to the resources"\n')
  f.write('  default     = {}\n')
  f.write('}\n')

