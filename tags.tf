locals {
  tags = merge(var.additional_tags, {
    Organization = local.source.Organization[0]
    Tribe        = lower(var.tribe)
    Squad        = lower(var.squad)
    team         = lower(var.squad) # Datadog uses team instead of Squad
    GEL_Service  = lower(var.gel_service)
    Service      = var.service
    Application  = var.application != "" ? var.application : var.service
    Environment  = lower(var.environment)
    env          = lower(var.environment) # Datadog uses env instead of Environment
    Provisioner  = "Terraform"
  })

  propagated_tags = [for key, value in local.tags :
    {
      key                 = key
      value               = value
      propagate_at_launch = true
    }
  ]

  datadog_env_vars = {
    DD_ENV         = lower(var.environment)
    DD_SERVICE     = var.service
    DD_APPLICATION = var.application != "" ? var.application : var.service
  }
}

output "tags" {
  value = local.tags
}

output "propagated_tags" {
  value = local.propagated_tags
}

output "datadog_env_vars" {
  value = local.datadog_env_vars
}
