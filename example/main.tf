module "tags" {
  source = "../"

  tribe       = "Evolving Genomic Healthcare"
  squad       = "Core Tech Team"
  gel_service = "Help clinicians order genomic tests"
  service     = "Example Service"
  application = "Service SubComponent"
  environment = "Dev"
  additional_tags = {
    MyTag = "MyValue"
  }
}

resource "local_file" "tag_policies" {
  filename = "tag_policies.json"
  content  = jsonencode(module.tags.tag_policies)
}

resource "local_file" "key_policy" {
  filename = "key_policy.json"
  content  = module.tags.keys_policy
}

resource "local_file" "default_tags" {
  filename = "default_tags.json"
  content  = jsonencode(module.tags.tags)
}

resource "local_file" "propagated_tags" {
  filename = "propagated_tags.json"
  content  = jsonencode(module.tags.propagated_tags)
}
