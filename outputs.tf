output "organizations" {
  value = local.source.Organization
}

output "organization" {
  value = local.source.Organization[0]
}

output "tribes" {
  value = local.source.Tribe
}

output "tribe" {
  value = var.tribe
}

output "gel_services" {
  value = local.source.GEL_Service
}

output "gel_service" {
  value = var.gel_service
}

output "squads" {
  value = local.source.Squad
}

output "squad" {
  value = var.squad
}

output "service" {
  value = var.service
}

output "application" {
  value = var.application
}

output "environments" {
  value = local.source.Environment
}

output "environment" {
  value = var.environment
}

output "additional_tags" {
  value = var.additional_tags
}
