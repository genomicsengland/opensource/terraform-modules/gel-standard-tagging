# AUTO-GENERATED FILE - DO NOT EDIT
# This file is generated using the "sync-tags.py" script

locals {
  source = {
    Organization = [
      "Genomics England Ltd",
    ]

    Tribe = [
      "cross tribe",
      "empowering our people",
      "engaging to build trust",
      "evolving genomic healthcare",
      "research ecosystem",
      "scalable tech",
      "the gel way",
    ]

    GEL_Service = [
      "best practise",
      "bioinformatics pipelines", # replaced by "healthcare genomics service" 18/10/2024
      "build research tools",     # replaced by "use research data" 01/10/2024
      "use research data",        # replaces "build research tools" 01/10/2024
      "cross tribe",
      "define and deliver scientific research",
      "delivering scalable tech",
      "engage research scientists",
      "healthcare genomics service",         # merge of "bioinformatics pipelines" & "help clinicians order genomic tests" 18/10/2024
      "help clinicians order genomic tests", # replaced by "help clinicians order genomic tests" 18/10/2024
      "help colleagues perform",
      "newborns",
      "trustworthy stewardship of genomic data",
    ]

    Squad = [
      "accelerating data exploration",
      "bioinformatics consulting team",
      "business systems team",
      "cancer dss",
      "cancer long reads squad",
      "cancer short reads squad",
      "clinical variant ark squad",
      "cohort management team",
      "commercial procurement team",
      "communications team",
      "core healthcare team",
      "core multi-modal team",
      "core newborns team",
      "core research tools team",
      "core tech team",
      "cri and airlock team",
      "cross tribe",
      "cyber security squad",
      "data infrastructure squad", # replaces "research data layer squad" 18/10/2024
      "data release team",         # replaces "research data layer squad" 18/10/2024
      "data workflows squad",      # replaces "research data products" & "research insight and facilitation" 18/10/2024
      "diverse data",
      "dss platform",
      "engage scientists core team",
      "engagement team",
      "enterprise data squad",
      "estates team",
      "ethics legal and ig team",
      "executive support team",
      "finance team",
      "healthcare test ordering squad",
      "imaging data products squad",
      "infrastructure services squad",
      "internal service management team",
      "knowledge management squad",
      "logistics team",
      "newborns intake squad",
      "partnership development team",
      "people team",
      "performance team",
      "pipeline core dataflows squad",
      "pipeline orchestration squad",
      "pipelines cross-squad",
      "quality team",
      "rare disease dss",
      "rare disease interpretation portal squad",
      "rare disease pipeline squad",
      "re platform squad",
      "research acquisition and processing squad",
      "research data layer squad", # replaced by "data infrastructure squad" & "data release team" 18/10/2024
      "research data products",
      "research insight and facilitation", # replaced by "data workflows squad" 18/10/2024
      "research management team",
      "sample and data management squad",
      "scientific rd team",
      "scientific research core team",
      "the gel way squad",
    ]

    Environment = [
      "core",
      "prod",
      "preprod",
      "uat",
      "e2e",
      "test",
      "dev",
      "sandbox",
    ]

  }
}
